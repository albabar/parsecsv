# === ParseCSV() :: Remote and Local CSV parser for PHP ===


It's very simple to use function.

Just include the `ParseCSV.php` file on your project header. And then call the function `ParseCSV()` with an input of string containing CSV file path or remote CSV file link.


``array PurseCSV(string $file)``

It returns an array containing values from CSV on success. Return false on fail.
