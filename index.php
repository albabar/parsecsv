<?php
/**
 * @package ParseCSV
 * @version 0.1
 */

/**
 * Usage Example of @ParseCSV() script.
 */
include("ParseCSV.php");
$file = "export.csv";

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Usage Example of ParseCSV Script</title>
</head>
<body>
<pre>
    <?php print_r(PurseCSV($file)); ?>
</pre>
</body>
</html>
