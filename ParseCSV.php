<?php
/**
 * @package ParseCSV
 * @version 0.1
 * @author Babar Al-Amin
 * @description Remote and local CSV parser for PHP. It doesn't use fopen() which may disabled on some hosting server.
 */

/**
 * @function ParseCSV()
 * @param string $file File path or URL
 * @return array or false on error
 */
function PurseCSV($file) {
    if(explode("?", pathinfo($file, PATHINFO_EXTENSION))[0] != "csv") return false;
    $csv_data = file_get_contents($file);
    preg_match_all("/[a-zA-Z0-9_\\ ,\@\.\-\'\"\(\):\/]+(,|\"|\b)/", $csv_data, $lines);
    $keys = explode(",", $lines[0][0]);
    $array_data = array();
    foreach(array_slice($lines[0], 1) as $line) {
        $items = preg_split("/,/", str_replace("\"","",$line));
        $array_data[] = array_combine($keys,$items);
    }
    return $array_data;
}
